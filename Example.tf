#  Using provider block to Initialize plugins for aws cloud

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.5.0"
    }
  }
}


provider "aws" {
region="us-east-1"
}


# Initializing S3 backend for the tf state file

terraform {
  backend "s3" {
    bucket = "terraform-s3-bucketjoy"
    key    = "terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-s3-bucketjoy-dynamodb"
  }
}

# using resource block to create a new resource

resource "aws_vpc" "terraform-terraforvpc" {
  cidr_block = "10.0.0.0/16"
  tags ={
    Name ="Terraform-VPC"
  }
}
resource "aws_subnet" "terraform-subnet1" {
  vpc_id     = aws_vpc.terraform-terraforvpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone ="us-east-1a"

  tags = {
    Name = "Terraform-subnet"
  }
}

# using data source block to reference an already existing resource that is not part of this code 

data "aws_vpc" "default_vpc" {
  default = true
}

resource "aws_subnet" "terraform-subnet2" {
  vpc_id     = data.aws_vpc.default_vpc.id
  cidr_block = var.cidr_block
  availability_zone = var.availability_zone
}

resource "aws_instance" "EC2-1" {
  ami           = var.ami
  instance_type = var.instance_type

  tags = {
    Name = "HelloWorld"
  }
}

variable "ami" {
  
}

variable "instance_type" {
  
}

# Using variable block to define and reference configuration values

variable "availability_zone" {
  default = "us-east-1a"
  
  
}
variable "cidr_block" {
  default = "172.31.128.0/20"
}

output "vpc-id" {
  value = aws_vpc.terraform-terraforvpc.id
}

output "subnet-1-az" {
  value = aws_subnet.terraform-subnet1.availability_zone
}

  output "subnet-2-cidr" {
    value = aws_subnet.terraform-subnet2.cidr_block
  }